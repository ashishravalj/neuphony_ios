//
//  IntroModle.swift
//  Neuphony
//
//  Created by DailyGate Informix on 09/12/20.
//

import UIKit
import IntroScreen

class IntroModle: NSObject {

    static let shared = IntroModle()
    
    
}


struct IntroModelDataSource: IntroDataSource {
    
    let numberOfPages = 3
    
    // Set to true, if you want to fade out the last page color into black.
    let fadeOutLastPage: Bool = false
    let brightness: CGFloat = 0.5
    
    func viewController(at index: Int) -> IntroPageViewController? {
        switch index {
        case 0:
            return DefaultIntroPageViewController(
                index: index,
                hue: 353.3/360,
                title: "First page",
                subtitle: "This is the first page.",
                image: UIImage(named: "first")!
            )
            
        case 1:
            return DefaultIntroPageViewController(
                index: index,
                hue: 10/360,
                title: "Second page",
                subtitle: "This is the second page.",
                image: UIImage(named: "second")!
            )
        case 2:
            return DefaultIntroPageViewController(
                index: index,
                hue: 355/360,
                title: "Third page",
                subtitle: "This is the third page.",
                image: UIImage(named: "third")!
            )
        default:
            return nil
        }
    }
}
