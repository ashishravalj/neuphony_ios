//
//  UserDefaultData.swift
//  Neuphony
//
//  Created by DailyGate Informix on 09/12/20.
//

import UIKit

extension UserDefaults {
    
    func setIsIntroComplete(isComplete:Bool){
        UserDefaults.standard.set(isComplete, forKey: "ISINTRODONE")
    }
    
    func getIsIntroComplete()->Bool{
       return UserDefaults.standard.bool(forKey: "ISINTRODONE")
    }

}
