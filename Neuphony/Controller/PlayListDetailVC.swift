//
//  PlayListDetailVC.swift
//  Neuphony
//
//  Created by APPLE on 12/12/20.
//

import UIKit

class PlayListDetailVC: UIViewController {

    @IBOutlet weak var imageViewHeader:UIImageView!
    @IBOutlet weak var playNowVW:UIView!
    @IBOutlet weak var imgplayNow:UIImageView!

    
    let headerViewMaxHeight: CGFloat = 180
    let headerViewMinHeight: CGFloat = 44 + UIApplication.shared.statusBarFrame.height
    @IBOutlet var headerViewHeightConstraint: NSLayoutConstraint!

    
    @IBOutlet weak var ObjTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        playNowVW.isHidden = true
        imgplayNow.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didClickBtnBack(_ sender:UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func playviaBLE(_ sender:UIButton){
     
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "BLEConnectionVC") as! BLEConnectionVC
        playNowVW.isHidden = true
        imgplayNow.isHidden = true
        self.navigationController?.pushViewController(VC, animated: true)
        
    }
    
    @IBAction func didClickBtnSetupBLE(_ sender:UIButton){
        
        playNowVW.isHidden = false
        imgplayNow.isHidden = false
        
        
    }
    
    @IBAction func didClickBtnSkip(_ sender:UIButton){
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "MusicPlayerVC") as! MusicPlayerVC
        playNowVW.isHidden = true
        imgplayNow.isHidden = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
}
//MARK:  CollectionView DataSource and Delegate

extension PlayListDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2//self.arrChatList.count == 0 ? 0 : self.arrChatList.count//self.arrayCart[section].items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayDetailListCustomCell", for: indexPath) as! PlayDetailListCustomCell
        
       
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let headerViewMinHeight: CGFloat = 44 + UIApplication.shared.statusBarFrame.height
        let yPos = self.ObjTableView.contentOffset.y
        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - yPos

        if newHeaderViewHeight > headerViewMaxHeight {
            // Here, Manage Your Score Format View
            print("y pos \(yPos)")
            headerViewHeightConstraint.constant = min(headerViewMaxHeight, newHeaderViewHeight)
           print("Top Max === \(newHeaderViewHeight) ====")
            self.imageViewHeader.isHidden = false
        } else if newHeaderViewHeight < headerViewMinHeight {
            print("y pos \(yPos)")
            print("Top === \(newHeaderViewHeight) ====")
            headerViewHeightConstraint.constant = headerViewMinHeight
            self.imageViewHeader.isHidden = true
        } else {
            print("y pos \(yPos)")
            print("Bottom === \(newHeaderViewHeight) ====")
            headerViewHeightConstraint.constant = newHeaderViewHeight
            scrollView.contentOffset.y = 0 // block scroll view
        }
    }
}
