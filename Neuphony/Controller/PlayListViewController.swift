//
//  PlayListViewController.swift
//  Neuphony
//
//  Created by APPLE on 12/12/20.
//

import UIKit

class PlayListViewController: UIViewController {

    
    @IBOutlet weak var imageViewHeader:UIImageView!

    let headerViewMaxHeight: CGFloat = 180
    let headerViewMinHeight: CGFloat = 66 + UIApplication.shared.statusBarFrame.height
    @IBOutlet var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var ObjChatListTableView: UITableView!
//    @IBOutlet weak var txtSearch: UITextField!
//    @IBOutlet weak var objSearchView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

//        objSearchView.layer.cornerRadius = objSearchView.frame.height / 2
//        objSearchView.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didClickBtnBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let headerViewMinHeight: CGFloat = 80

        let yPos = self.ObjChatListTableView.contentOffset.y
        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - yPos

        if newHeaderViewHeight > headerViewMaxHeight {
            // Here, Manage Your Score Format View
//          print("y pos \(yPos)")
            headerViewHeightConstraint.constant = min(headerViewMaxHeight, newHeaderViewHeight)
//          print("Top Max === \(newHeaderViewHeight) ====")
            self.imageViewHeader.isHidden = false

        } else if newHeaderViewHeight < headerViewMinHeight {
            print("y pos \(yPos)")
            print("Top === \(newHeaderViewHeight) ====")
            headerViewHeightConstraint.constant = headerViewMinHeight
            self.imageViewHeader.isHidden = true
            
        } else {
//            print("y pos \(yPos)")
//            print("Bottom === \(newHeaderViewHeight) ====")
            headerViewHeightConstraint.constant = newHeaderViewHeight
            scrollView.contentOffset.y = 0 // block scroll view
            self.imageViewHeader.isHidden = false

        }
    }
}
extension PlayListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayListCustomCell", for: indexPath) as! PlayListCustomCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "PlayListDetailVC") as! PlayListDetailVC
        self.navigationController?.pushViewController(VC, animated: true)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
