//
//  CheckingSensorVC.swift
//  Neuphony
//
//  Created by DailyGate Informix on 13/12/20.
//

import UIKit

class CheckingSensorVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func didClickBtnSetupBrainMap(_ sender:UIButton){
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "BrainMapVC") as! BrainMapVC
        self.navigationController?.pushViewController(VC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
