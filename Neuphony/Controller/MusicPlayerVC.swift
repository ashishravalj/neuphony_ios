//
//  MusicPlayerVC.swift
//  Neuphony
//
//  Created by DailyGate Informix on 26/12/20.
//

import UIKit
import SoundWave

class MusicPlayerVC: UIViewController {
    
    @IBOutlet weak var  lblRunningTime:UILabel!
    @IBOutlet weak var  lblTotalTime:UILabel!
    @IBOutlet weak var  musicInfoBaseView:UIView!
    @IBOutlet weak var  musicInfoImageView:UIImageView!
    @IBOutlet weak var  btnFav:UIButton!
    @IBOutlet weak var  btnSetting:UIButton!
    @IBOutlet weak var  btnPlay:UIButton!
    
    @IBOutlet weak var audioVisualizationView:AudioVisualizationView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.musicInfoBaseView.layer.cornerRadius = 75
        self.musicInfoBaseView.layer.masksToBounds = true
        
        self.audioVisualizationView.meteringLevelBarWidth = 5.0
        self.audioVisualizationView.meteringLevelBarInterItem = 1.0
        self.audioVisualizationView.meteringLevelBarCornerRadius = 0.0
//        self.audioVisualizationView.meter = true
        self.audioVisualizationView.gradientStartColor = .white
        self.audioVisualizationView.gradientEndColor = .black
        self.audioVisualizationView.audioVisualizationMode = .write
        self.audioVisualizationView.add(meteringLevel: 0.6)

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func didClickbtnPlay(_ sender:UIButton){
        
    }
    
    @IBAction func didClickbtnFavoirite(_ sender:UIButton){
        
    }
    
    @IBAction func didClickbtnSetting(_ sender:UIButton){
        
    }
    

    

}
