//
//  ViewController.swift
//  Neuphony
//
//  Created by DailyGate Informix on 09/12/20.
//

import UIKit
import EAIntroView

class IntroVC: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var imageViewHeader:UIImageView!
    @IBOutlet weak var navHeaderView:UIView!

    
    let arrSection = ["CATEGORY","TOP SOUNDS","POPULAR SOUNDS","RECENTLY PLAYED","SLEEP STORIES"]
    
    let headerViewMaxHeight: CGFloat = 143
    let headerViewMinHeight: CGFloat = 85 + UIApplication.shared.statusBarFrame.height
    
    var introView : EAIntroView!
    
    @IBOutlet var headerViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navHeaderView.isHidden = true        
        // Do any additional setup after loading the view.
        
        
        tableView.tableFooterView = UIView.init()
        
        tableView.register(UINib(nibName: "CategoryTableCell", bundle: nil), forCellReuseIdentifier: "CategoryTableCellID")
        
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        
        if UserDefaults.standard.getIsIntroComplete() == false{
//            DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                self.setupIntroView()
            self.tabBarController?.tabBar.isHidden = true
//            }
        }
        
    }
    
    
    func setupIntroView(){
        let viewIntro1 = IntroView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        viewIntro1.imageView.image = #imageLiteral(resourceName: "intro")
        viewIntro1.lblTitle.text = "Discover the time you didn't have".uppercased()
        viewIntro1.lblDescription.text = "Neuphony trains your brain to deal better with daily distractions & help you become more efficient."
        viewIntro1.lblTitle.textColor = .white
        viewIntro1.lblDescription.textColor = .white
        viewIntro1.btnGetStarted.isHidden = true
        viewIntro1.contentView.backgroundColor = UIColor.init(hexString: "3A2050")
        
        
        let viewIntro2 = IntroView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        viewIntro2.imageView.image = #imageLiteral(resourceName: "intro2")
        viewIntro2.lblTitle.text = "ITS ALL IN THE MIND !".uppercased()
        viewIntro2.lblDescription.text = "Our brain is the most important and complex part of our body. With Neuphony Change the reactive approach towards mental health and start building resilience by being more proactive."
        viewIntro2.lblTitle.textColor = .white
        viewIntro2.lblDescription.textColor = .white
        viewIntro2.btnGetStarted.isHidden = true
        viewIntro2.contentView.backgroundColor = UIColor.init(hexString: "2C757B")
        
        let viewIntro3 = IntroView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        viewIntro3.imageView.image = #imageLiteral(resourceName: "intro3")
        viewIntro3.lblTitle.text = "Listen to your Brain".uppercased()
        viewIntro3.lblDescription.text = "Neuphony understand your Brain and behavior better & helps you to Improve focus, Reduce anxiety and stress develop gratitude."
        viewIntro3.btnGetStarted.layer.cornerRadius = 27.5
        viewIntro3.btnGetStarted.layer.masksToBounds  = true
        viewIntro3.lblTitle.textColor = .black
        viewIntro3.lblDescription.textColor = .black
        viewIntro3.btnGetStarted.addTarget(self, action: #selector(self.didClickBtnGetStarted(_:)), for: .touchUpInside)
        viewIntro3.contentView.backgroundColor = UIColor.init(hexString:"FFB75A")
        
        
        let intro1 = EAIntroPage.init()
        intro1.bgColor = .red
        intro1.customView = viewIntro1
        
        
        let intro2 = EAIntroPage.init()
        intro2.customView = viewIntro2
        
        let intro3 = EAIntroPage.init()
        intro3.customView = viewIntro3
        
        
         introView = EAIntroView.init(frame: self.view.bounds, andPages: [intro1,intro2,intro3])
        introView?.delegate = self
        introView?.skipButton.isHidden = true
//        introView?.pageControl.isHidden = true
        introView?.swipeToExit = false
        introView.scrollView.bounces = false
        introView?.show(in: self.view, animateDuration: 0.0)
    }
    
    //MARK: Buttons Events
    
    @IBAction func didClickBtnHome(_ sender:UIButton){
        
    }
    
    @IBAction func didClickBtnMaditation(_ sender:UIButton){
        
    }
    
    @IBAction func didClickBtnInsight(_ sender:UIButton){
        
    }
    
    @objc func didClickBtnGetStarted(_ sender:UIButton){
        self.tabBarController?.tabBar.isHidden = false
        self.dismiss(animated: true, completion: nil)
        self.introView.hide(withFadeOutDuration: 0.2)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let headerViewMinHeight: CGFloat = 85 + UIApplication.shared.statusBarFrame.height

        let yPos = tableView.contentOffset.y
        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - yPos
        
        
        if newHeaderViewHeight > headerViewMaxHeight {
            // Here, Manage Your Score Format View
            self.navHeaderView.fadeOut(completion: {
                (finished: Bool) -> Void in
                self.navHeaderView.isHidden = true
            })
            print("y pos \(yPos)")
            headerViewHeightConstraint.constant = max(headerViewMaxHeight, newHeaderViewHeight)
           print("Top Max === \(newHeaderViewHeight) ====")
            //self.imageViewHeader.isHidden = false
            
            self.imageViewHeader.fadeIn(completion: {
                (finished: Bool) -> Void in
                self.imageViewHeader.isHidden = false
            })
            

        } else if newHeaderViewHeight < headerViewMinHeight {

            print("y pos \(yPos)")
            print("Top === \(newHeaderViewHeight) ====")
            headerViewHeightConstraint.constant = headerViewMinHeight
            
            
            self.imageViewHeader.fadeOut(completion: {
                (finished: Bool) -> Void in
                self.imageViewHeader.isHidden = true
            })
            
            
            self.navHeaderView.fadeIn(completion: {
                (finished: Bool) -> Void in
                self.navHeaderView.isHidden = false
            })
            
        }
        else {

            self.navHeaderView.fadeOut(completion: {
                (finished: Bool) -> Void in
                self.navHeaderView.isHidden = true
            })
            
            
            self.imageViewHeader.fadeIn(completion: {
                (finished: Bool) -> Void in
                self.imageViewHeader.isHidden = false
            })
            
            
            print("y pos \(yPos)")
            print("Bottom === \(newHeaderViewHeight) ====")
            headerViewHeightConstraint.constant = newHeaderViewHeight
            scrollView.contentOffset.y = 0 // block scroll view
            //self.imageViewHeader.isHidden = false

        }
    }
    
}

extension IntroVC : EAIntroDelegate{
    func introWillFinish(_ introView: EAIntroView!, wasSkipped: Bool) {
//        self.view.backgroundColor = .gray
        UserDefaults.standard.setIsIntroComplete(isComplete: true)
    }
}


extension IntroVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSection.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCellID") as! CategoryTableCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
        headerView.backgroundColor = .white
        
        let label = UILabel()
        label.frame = CGRect.init(x: 15, y: 2, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = arrSection[section]
        label.font = FontBook.Regular.of(style: .Body).withSize(16) // my custom font
        label.textColor = .black // my custom colour
        
        
        headerView.addSubview(label)
        
        
        let label1 = UILabel()
        label1.frame = CGRect.init(x: 15, y: label.frame.height+5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label1.text = "Learning how to control your focus and avoid distractions"
        label1.lineBreakMode = .byWordWrapping
        label1.numberOfLines = 0
        label1.font = FontBook.Regular.of(style: .Body).withSize(14) // my custom font
        label1.textColor = .lightGray // my custom colour
        headerView.addSubview(label1)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
}


extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
extension UIView {

    func fadeIn(_ duration: TimeInterval = 0.0, delay: TimeInterval = 0.5, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
    }, completion: completion)  }

    func fadeOut(_ duration: TimeInterval = 0.0, delay: TimeInterval = 0.5, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
    }, completion: completion)
   }
}
