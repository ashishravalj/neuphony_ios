//
//  MaditationVC.swift
//  Neuphony
//
//  Created by DailyGate Informix on 12/12/20.
//

import UIKit

class MaditationVC: UIViewController {
    @IBOutlet weak var collectionView:UICollectionView!
    
    @IBOutlet weak var navHeaderView:UIView!

    @IBOutlet weak var imageViewHeader:UIImageView!
    let headerViewMaxHeight: CGFloat = 180
    let headerViewMinHeight: CGFloat = 44 + UIApplication.shared.statusBarFrame.height
    @IBOutlet var headerViewHeightConstraint: NSLayoutConstraint!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionViewInsets()
        setupLayout()
        self.tabBarController?.hidesBottomBarWhenPushed = false
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    //MARK: private
    
    private func setupCollectionViewInsets() {
        collectionView!.backgroundColor = .clear
        collectionView!.contentInset = UIEdgeInsets(
            top: 15,
            left: 5,
            bottom: 49,
            right: 5
        )
    }
    
    private func setupLayout() {
        let layout: PinterestLayout = {
            if let layout = collectionView.collectionViewLayout as? PinterestLayout {
                return layout
            }
            let layout = PinterestLayout()
            
            collectionView?.collectionViewLayout = layout
            
            return layout
        }()
        layout.delegate = self
        layout.cellPadding = 5
        layout.numberOfColumns = 2
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let headerViewMinHeight: CGFloat = 66 + UIApplication.shared.statusBarFrame.height

        let yPos = self.collectionView.contentOffset.y
        let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - yPos
        
        
        if newHeaderViewHeight > headerViewMaxHeight {
            // Here, Manage Your Score Format View
            self.navHeaderView.fadeOut(completion: {
                (finished: Bool) -> Void in
                self.navHeaderView.isHidden = true
            })
            print("y pos \(yPos)")
            headerViewHeightConstraint.constant = max(headerViewMaxHeight, newHeaderViewHeight)
           print("Top Max === \(newHeaderViewHeight) ====")
            //self.imageViewHeader.isHidden = false
            
            self.imageViewHeader.fadeIn(completion: {
                (finished: Bool) -> Void in
                self.imageViewHeader.isHidden = false
            })
            

        } else if newHeaderViewHeight < headerViewMinHeight {

            print("y pos \(yPos)")
            print("Top === \(newHeaderViewHeight) ====")
            headerViewHeightConstraint.constant = headerViewMinHeight
            
            
            self.imageViewHeader.fadeOut(completion: {
                (finished: Bool) -> Void in
                self.imageViewHeader.isHidden = true
            })
            
            
            self.navHeaderView.fadeIn(completion: {
                (finished: Bool) -> Void in
                self.navHeaderView.isHidden = false
            })
            
        }
        else {

            self.navHeaderView.fadeOut(completion: {
                (finished: Bool) -> Void in
                self.navHeaderView.isHidden = true
            })
            
            
            self.imageViewHeader.fadeIn(completion: {
                (finished: Bool) -> Void in
                self.imageViewHeader.isHidden = false
            })
            
            
            print("y pos \(yPos)")
            print("Bottom === \(newHeaderViewHeight) ====")
            headerViewHeightConstraint.constant = newHeaderViewHeight
            scrollView.contentOffset.y = 0 // block scroll view
            //self.imageViewHeader.isHidden = false

        }
    }
    
    
}


//MARK: UICollectionViewDataSource

extension MaditationVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "MeditationCellID",
            for: indexPath) as! MeditationCell

        cell.baseView.layer.cornerRadius = 5.0
        cell.baseView.layer.masksToBounds = true
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {       
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "PlayListViewController") as! PlayListViewController
        self.navigationController?.pushViewController(VC, animated: true)
    }
}


//MARK: PinterestLayoutDelegate

extension MaditationVC: PinterestLayoutDelegate {
    
    func collectionView(collectionView: UICollectionView,
                        heightForImageAtIndexPath indexPath: IndexPath,
                        withWidth: CGFloat) -> CGFloat {
//        let image = images[indexPath.item]
//
//        return image.height(forWidth: withWidth)
        
        if (indexPath.row + 1) % 2 == 0{
            return 250
        }else{
            return 210
        }
    }
    
    func collectionView(collectionView: UICollectionView,
                        heightForAnnotationAtIndexPath indexPath: IndexPath,
                        withWidth: CGFloat) -> CGFloat {
        return 0
    }
}


