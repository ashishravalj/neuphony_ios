//
//  MeditationCell.swift
//  Neuphony
//
//  Created by DailyGate Informix on 13/12/20.
//

import UIKit

class MeditationCell: UICollectionViewCell {
    
    @IBOutlet weak var btnFav:UIButton!
    @IBOutlet weak var baseView:UIView!
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    
}
