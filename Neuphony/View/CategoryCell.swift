//
//  CategoryCell.swift
//  Neuphony
//
//  Created by DailyGate Informix on 09/12/20.
//

import UIKit

class CategoryCell: UICollectionViewCell {

    @IBOutlet weak var headerImageView:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var baseView:customeView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
