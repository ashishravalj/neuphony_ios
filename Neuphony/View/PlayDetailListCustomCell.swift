//
//  PlayDetailListCustomCell.swift
//  Neuphony
//
//  Created by APPLE on 12/12/20.
//

import UIKit

class PlayDetailListCustomCell: UITableViewCell {

    @IBOutlet weak var objCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.objCollectionView.register(UINib(nibName: "SessionCustomCell", bundle: nil), forCellWithReuseIdentifier: "SessionCustomCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func roundedCorners(corners : UIRectCorner, radius : CGFloat) {
           let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
           let mask = CAShapeLayer()
           mask.path = path.cgPath
           layer.mask = mask
       }

}
extension PlayDetailListCustomCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
            return 5//self.objPatient.images.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
            let cell:SessionCustomCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SessionCustomCell", for: indexPath) as! SessionCustomCell
        cell.objView.layer.cornerRadius = cell.objView.frame.height / 3
        

        if indexPath.item == 0 {
            cell.objView.backgroundColor = UIColor(red: 0.184, green: 0.71, blue: 0.78, alpha: 0.57)
        } else {
            cell.objView.layer.backgroundColor = UIColor(red: 0.769, green: 0.769, blue: 0.769, alpha: 0.62).cgColor
        }
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width:(objCollectionView.bounds.width / 3) - 5, height:80)
    }
}
