//
//  IntroView.swift
//  Neuphony
//
//  Created by DailyGate Informix on 19/12/20.
//

import UIKit

class IntroView: UIView {
    
    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var btnGetStarted:UIButton!
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
    

    private func commonInit(){
        Bundle.main.loadNibNamed("IntroView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
    }

}
