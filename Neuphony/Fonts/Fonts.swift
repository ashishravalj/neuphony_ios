//
//  Fonts.swift
//  Neuphony
//
//  Created by DailyGate Informix on 09/12/20.
//

import Foundation
import UIKit

enum DynamicType : String {
  case Body = "UIFontTextStyleBody"
  case Headline = "UIFontTextStyleHeadline"
  case Subheadline = "UIFontTextStyleSubheadline"
  case Footnote = "UIFontTextStyleFootnote"
  case Caption1 = "UIFontTextStyleCaption1"
  case Caption2 = "UIFontTextStyleCaption2"
}

enum FontBook: String {
  case Regular = "Montserrat-Medium"
  case Bold = "Montserrat-Bold"
    case Black = "Montserrat-Black"
    
  func of(style: DynamicType) -> UIFont {
    let preferred = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: style.rawValue)).pointSize
    return UIFont(name: self.rawValue, size: preferred)!
  }
}
